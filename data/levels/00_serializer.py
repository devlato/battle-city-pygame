#!/usr/bin/python

import pickle
import pprint

# The Level Dictionary 
level = { 
	# Level name
	'name': 'Level 01', 
	# Level background color
	'background': (16, 16, 16), 
	# Types of blocks used in the level
	'block_types': { 
		# The block designation on the map
		'=': {
			# The block name
			'name': 'brick',
			# The file name of the appropriate image 
			'file': 'brick.png', 
			# Image size in grid cell units
			'size': (1, 1), 
			# If block be ignored while tank is moving
			'is_transparent': False, 
			# If block can be destroyed
			'is_destroyable': True, 
			# How much power tank needs to destroy it
			'bullets_to_destroy': 1
		}, 
		'#': {
			'name': 'stone', 
			'file': 'stone.png', 
			'size': (1, 1), 
			'is_transparent': False, 
			'is_destroyable': True, 
			'bullets_to_destroy': 3
		}, 
		'@': {
			'name': 'grass', 
			'file': 'grass.png', 
			'size': (1, 1), 
			'is_transparent': True, 
			'is_destroyable': False, 
			'bullets_to_destroy': 0
		}
	}, 
	# The level map
	'map': { 
		# The character which is used to mark a free space
		'space': '.', 
		# The map
		'data': [
			list('..........................'), 
			list('..........................'),
			list('..==..==..==..==..==..==..'), 
			list('..==..==..==..==..==..==..'), 
			list('..==..==..==..==..==..==..'), 
			list('..==..==..==..==..==..==..'), 
			list('..==..==..==##==..==..==..'), 
			list('..==..==..==##==..==..==..'), 
			list('@@==..==..==..==..==..==..'), 
			list('@@==..==..==..==..==..==..'), 
			list('@@........==..==..........'), 
			list('@@........==..==..........'), 
			list('....====..........====....'), 
			list('##..====..........====..##'), 
			list('..........==..==..........'), 
			list('..........======..........'), 
			list('..==..==..======..==..==..'), 
			list('..==..==..==..==..==..==..'), 
			list('..==..==..==..==..==..==..'), 
			list('..==..==..==..==..==..==..'), 
			list('..==..==..........==..==..'), 
			list('..==..==..........==..==..'), 
			list('..==..==...====...==..==..'), 
			list('...........=..=...........'), 
			list('...........=..=...........') 
		], 
	}, 
	# THe grid parameters
	'grid': { 
		# The grid step (size in pixels)
		'step': (16, 16), 
		# The grid size in grid cells
		'size': (25, 26) 
	}, 
	# User tank parameters
	'user': { 
		# Name
		'name': 'user', 
		# Image file
		'file': 'usertank.png', 
		# Size in a grid cells
		'size': (2, 2), 
		# Start position on the map
		'position': (23, 8), 
		# Orientation angle
		'angle': 0, 
		# Life count on this level
		'life_count': 3, 
		# Shot power
		'power': 1, 
		# User tank's bullet
		'bullet': { 
			# Name
			'name': 'single bullet', 
			'direction': None, 
			# Angle 
			'angle': None, 
			# Position on the map
			'position': None, 
			# Power 
			'power': 1, 
			# Speed
			'speed': 1 
		} 
	}, 
	# Bullet in common
	'bullet': { 
		# Name
		'name': 'bullet', 
		# Image file
		'file': 'bullet.png',  
		# Size in a grid cells
		'size': (2, 2)
	}, 
	# User's fortress, enemy's target
	'target': { 
		# Name
		'name': 'target', 
		# Image file
		'file': 'target.png', 
		# Size in a grid cells
		'size': (2, 2), 
		# Position on the map
		'position': (23, 12) 
	}, 
	# Enemy types 
	'enemy_types': { 
		# Enemy type descriptor
		'easy': { 
			# Name
			'name': 'easy tank', 
			# Image file
			'file': 'enemytank_easy.png', 
			# Size in a grid cells
			'size': (2, 2), 
			# Maximum count of this kind of enemies
			'max_amount': 11, 
			# Shot power
			'power': 1, 
			# Count of bullets to destroy this enemy unit
			'bullets_to_destroy': 1, 
		}, 
		'medium': { 
			'name': 'medium tank', 
			'file': 'enemytank_medium.png', 
			'size': (2, 2), 
			'max_amount': 5, 
			'power': 2, 
			'bullets_to_destroy': 2, 
		}, 
		'hard': { 
			'name': 'hard tank', 
			'file': 'enemytank_hard.png', 
			'size': (2, 2), 
			'max_amount': 2, 
			'power': 3, 
			'bullets_to_destroy': 3, 
		}
	}, 
	# Enemy instances
	'enemies': {
		'max_concurrent_amount': 3, 
		'generated': None, 
		'data': [
			# {
			# 	# Type of an enemy 
			# 	'type': None, 
			# 	# Position of an enemy
			# 	'position': None, 
			# 	# Orientation of an enemy
			# 	'angle': None
			# 	# Info about enemy bullet
			# 	'bullet': { 
			# 		# Bullet position
			# 		'position': None,
			# 		# Bullet direction
			# 		'angle': None 
			# 	}
			# }
		]
	}
}

output = open('level.lvl', 'wb')
pickle.dump(level, output)
output.close()
