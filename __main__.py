#!/usr/bin/python

import sys
import os
import pygame
from pygame.locals import * 
import pickle
import pprint
import random
import dircache

game_title = 'Battle City' 

resource_path = 'data'
IMAGE_PATH = 'images'
LEVEL_PATH = 'levels'

KEY_REPEAT_DELAY = 500 
KEY_REPEAT_INTERVAL = 40
FRAME_RATE = 40
MILLISECONDS_PER_FRAME = 1000 / FRAME_RATE
TANK_GENERATOR_DELAY = 1000
TANK_ACTION_DELAY = 160

BULLET_ANIMATION_TIMER_EVENT = USEREVENT + 1
TANKS_ANIMATION_TIMER_EVENT = USEREVENT + 2
TANK_GENERATOR_EVENT = USEREVENT + 3
ENEMY_BULLET_ANIMATION_TIMER_EVENT = USEREVENT + 4

IMAGE_PATH = os.path.join(resource_path, IMAGE_PATH)
LEVEL_PATH = os.path.join(resource_path, LEVEL_PATH)

def load_level_list():
	print "Please select the level: "
	dir = dircache.listdir(LEVEL_PATH)
	lst = []
	for d in dir: 
		basename, extension = d.split('.')
		if extension == 'lvl': 
			#print "> ", basename
			lst.append(basename)
	return lst

def load_level(level_file_name): 
	level_file_name += '.lvl'
	level_file = open(os.path.join(LEVEL_PATH, level_file_name), 'rb') 
	level_data = pickle.load(level_file)
	level_file.close()
	return level_data

def load_image(name, colorkey = None): 
	fullname = os.path.join(IMAGE_PATH, name)
	try:
		image = pygame.image.load(fullname)
	except pygame.error, message:
		print "Failed to load an image"
		raise SystemExit, message
	image = image.convert()
	if colorkey is not None: 
		if colorkey == -1:
			colorkey = image.get_at((0, 0))
		image.set_colorkey(colorkey, RLEACCEL)
	return image, image.get_rect()

def load_parts_images(level_data, part_name, colorKey = None): 
	for element in level_data[part_name]: 
		# pprint.pprint(element)
		# element['image'], element['image_rect'] = load_image(element['file'], colorKey)
		level_data[part_name][element]['image'], level_data[part_name][element]['image_rect'] = load_image(level_data[part_name][element]['file'], colorKey)
	return level_data

def load_single_part_image(level_data, part_name, colorKey = None): 
	level_data[part_name]['image'], level_data[part_name]['image_rect'] = load_image(level_data[part_name]['file'], -1)

def load_enemy_types_images(level_data): 
	return load_parts_images(level_data, 'enemy_types', -1)

def load_block_type_images(level_data): 
	return load_parts_images(level_data, 'block_types', -1)

def load_user_image(level_data): 
	return load_single_part_image(level_data, 'user', -1)

def load_target_image(level_data): 
	return load_single_part_image(level_data, 'target', -1)

def load_bullet_image(level_data): 
	return load_single_part_image(level_data, 'bullet', -1)

def load_level_object_images(level_data):
	load_enemy_types_images(level_data)
	load_block_type_images(level_data)
	load_user_image(level_data)
	load_target_image(level_data)
	load_bullet_image(level_data)

def get_map_size(level_data): 
	height = len(level_data['map']['data'])
	width = len(level_data['map']['data'][0])
	return (height, width)

def get_map_width(level_data): 
	height, width = get_map_size(level_data)
	return width

def get_map_height(level_data): 
	height, width = get_map_size(level_data)
	return height

def get_grid_size(level_data): 
	return level_data['grid']['size']

def get_grid_width(level_data): 
	height, width = get_grid_size(level_data)
	return width

def get_grid_height(level_data): 
	height, width = get_grid_size(level_data)
	return height

def get_grid_step_size(level_data): 
	return level_data['grid']['step']

def get_grid_step_width(level_data): 
	height, width = get_grid_step_size(level_data)
	return width

def get_grid_step_height(level_data): 
	height, width = get_grid_step_size(level_data)
	return height

def get_user_tank_size(level_data): 
	return level_data['user']['size']

def get_user_tank_width(level_data): 
	height, width = get_user_tank_size(level_data)
	return width

def get_user_tank_height(level_data): 
	height, width = get_user_tank_size(level_data)
	return height

def get_enemy_tank_size(level_data, enemy_type): 
	return level_data['enemy_types'][enemy_type]['size']

def get_enemy_tank_width(level_data, enemy_type): 
	height, width = get_enemy_tank_size(level_data, enemy_type)
	return width

def get_enemy_tank_height(level_data, enemy_type): 
	height, width = get_enemy_tank_size(level_data, enemy_type)
	return height

def get_bullet_size(level_data): 
	return level_data['bullet']['size']

def get_bullet_width(level_data): 
	height, width = get_bullet_size(level_data)
	return width

def get_bullet_height(level_data): 
	height, width = get_bullet_size(level_data)
	return height

def get_target_size(level_data): 
	return level_data['target']['size']

def get_target_width(level_data): 
	height, width = get_target_size(level_data)
	return width

def get_target_height(level_data): 
	height, width = get_target_size(level_data)
	return height

def draw_grid_element(level_data, screen, image, (y, x)): 
	screen.blit(image, (x * get_grid_step_width(level_data), y * get_grid_step_height(level_data))) 
	return level_data

def draw_map(level_data, screen): 
	y = 0
	for map_line in level_data['map']['data']: 
		x = 0
		for map_cell in map_line: 
			if map_cell is not level_data['map']['space'] and map_cell in level_data['block_types']: 
				draw_grid_element(level_data, screen, level_data['block_types'][map_cell]['image'], (y, x))
			x += 1
		y += 1
	return level_data

def draw_target(level_data, screen): 
	draw_grid_element(level_data, screen, level_data['target']['image'], level_data['target']['position'])
	return level_data

def draw_user_bullet(level_data, screen): 
	if level_data['user']['bullet']['position'] is not None:
		draw_grid_element(level_data, screen, level_data['bullet']['image'], level_data['user']['bullet']['position'])
	return level_data

def draw_enemy_bullets(level_data, screen): 
	max_amount = level_data['enemies']['max_concurrent_amount']
	if len(level_data['enemies']['data']) > 0: 
		for i in range(0, max_amount): 
			if level_data['enemies']['data'][i]['bullet']['position'] is not None:
				draw_grid_element(level_data, screen, level_data['bullet']['image'], level_data['enemies']['data'][i]['bullet']['position']) 
	return level_data

def draw_user(level_data, screen): 
	image = pygame.transform.rotate(level_data['user']['image'], level_data['user']['angle'])
	draw_grid_element(level_data, screen, image, level_data['user']['position'])
	return level_data

def draw_enemy_tanks(level_data, screen): 
	max_amount = level_data['enemies']['max_concurrent_amount']
	if len(level_data['enemies']['data']) > 0: 
		for i in range(0, max_amount): 
			type = level_data['enemies']['data'][i]['type']
			image = pygame.transform.rotate(level_data['enemy_types'][type]['image'], level_data['enemies']['data'][i]['angle'])
			draw_grid_element(level_data, screen, image, level_data['enemies']['data'][i]['position'])
	return level_data

def draw(level_data): 
	screen = pygame.display.get_surface()
	background = pygame.Surface(screen.get_size())
	background = background.convert()
	background.fill(level_data['background'])
	screen.blit(background, (0, 0))
	
	draw_target(level_data, screen)
	draw_user_bullet(level_data, screen)
	draw_user(level_data, screen)
	draw_enemy_bullets(level_data, screen)
	draw_enemy_tanks(level_data, screen)
	draw_map(level_data, screen)
	
	text = level_data['text']
	font = pygame.font.Font(None, 20)
	text = font.render(text, True, (255, 242, 0))
	textpos = text.get_rect()
	textpos.centerx = screen.get_rect().centerx
	textpos.centery = 20
	screen.blit(text, textpos)
	
	pygame.display.flip()
	return screen

def try_move_user_tank(level_data, position):
	delta_y, delta_x = position
	position_y, position_x = level_data['user']['position']
	if delta_y == -1:
		angle = 0
	elif delta_y == 1:
		angle = 180
	if delta_x == -1:
		angle = 90
	elif delta_x == 1:
		angle = -90
	level_data['user']['angle'] = angle
	new_position_y = position_y + delta_y
	new_position_x = position_x + delta_x
	
	if level_data['enemies']['generated'] is not None:
		if len(level_data['enemies']['data']) > 0: 
			max_amount = level_data['enemies']['max_concurrent_amount']
			for i in range(0, max_amount): 
				enemy_position_y, enemy_position_x = level_data['enemies']['data'][i]['position']
				enemy_type = level_data['enemies']['data'][i]['type']
				enemy_size_y, enemy_size_x = level_data['enemy_types'][enemy_type]['size']
				if (new_position_y >= enemy_position_y - 1) and (new_position_y < enemy_position_y + enemy_size_y) \
						and (new_position_x >= enemy_position_x - 1) and (new_position_x < enemy_position_x + enemy_size_x): 
					new_position_y, new_position_x = position_y, position_x
	
	if (new_position_y >= 0) and (new_position_y <= get_map_height(level_data) - get_user_tank_height(level_data)) \
			and (new_position_x >= 0) and (new_position_x <= get_map_width(level_data) - get_user_tank_width(level_data)):
			can_be_moved = True
			for y in range(new_position_y, new_position_y + get_user_tank_height(level_data)):
				for x in range(new_position_x, new_position_x + get_user_tank_width(level_data)): 
					map_cell = level_data['map']['data'][y][x]
					if map_cell in level_data['block_types'] \
							and not level_data['block_types'][map_cell]['is_transparent']:
						can_be_moved = False
						break 
			if can_be_moved:
				level_data['user']['position'] = (new_position_y, new_position_x)
	return level_data

def has_common_subregions(a_position, a_size, b_position, b_size): 
	a_position_y, a_position_x = a_position
	b_position_y, b_position_x = b_position
	a_size_y, a_size_x = a_size
	b_size_y, b_size_x = b_size
	a = Rect((a_position_x, a_position_y), (a_size_x, a_size_y))
	b = Rect((b_position_x, b_position_y), (b_size_x, b_size_y))
	return a.colliderect(b)

def generate_enemy_tanks(level_data): 
	map_height = get_map_height(level_data)
	map_width = get_map_width(level_data)
	count = 0
	for enemy_type in level_data['enemy_types']: 
		for i in range(0, level_data['enemy_types'][enemy_type]['max_amount']): 
			while True:
				position_y = random.randrange(0, map_height)
				position_x = random.randrange(0, map_width)
				if has_common_subregions((position_y, position_x), level_data['enemy_types'][enemy_type]['size'], \
						get_target_size(level_data), level_data['target']['position']): 
					continue
				if has_common_subregions((position_y, position_x), level_data['enemy_types'][enemy_type]['size'], \
						get_user_tank_size(level_data), level_data['user']['position']): 
					continue
				overlaps_with_previous = False
				for j in range(0, len(level_data['enemies']['data'])): 
					current_enemy_type = level_data['enemies']['data'][j]['type']
					if has_common_subregions(level_data['enemies']['data'][j]['position'], level_data['enemy_types'][current_enemy_type]['size'], 
							(position_y, position_x), level_data['enemy_types'][enemy_type]['size']):
						overlaps_with_previous = True
						break
				if overlaps_with_previous: 
					continue
				if has_common_subregions((position_y, position_x), level_data['enemy_types'][enemy_type]['size'], \
						(0, 0), (map_height - get_enemy_tank_height(level_data, enemy_type), map_width - get_enemy_tank_width(level_data, enemy_type))): 
					can_be_placed = True
					for y in range(position_y, position_y + get_enemy_tank_height(level_data, enemy_type)):
						for x in range(position_x, position_x + get_enemy_tank_width(level_data, enemy_type)): 
							map_cell = level_data['map']['data'][y][x]
							if map_cell in level_data['block_types'] \
									and not level_data['block_types'][map_cell]['is_transparent']:
								can_be_placed = False
								break 
					if can_be_placed:
						break
			angle = random.choice([0, 90, -90, 180])
			enemy = { 
				'type': enemy_type, 
				'position': (position_y, position_x), 
				'angle': angle, 
				'power': None, 
				'bullet': { 
					'position': None, 
					'angle': None
				}
			}
			level_data['enemies']['data'].append(enemy) 
		count += 1
	random.shuffle(level_data['enemies']['data'])
	level_data['enemies']['generated'] = count
	pygame.time.set_timer(TANK_GENERATOR_EVENT, 0)
	return level_data

def move_enemy_tanks(level_data):
	map_height = get_map_height(level_data)
	map_width = get_map_width(level_data)
	max_amount = level_data['enemies']['max_concurrent_amount']
	if level_data['enemies']['generated'] is not None:
		if len(level_data['enemies']['data']) > 0: 
			for i in range(0, max_amount): 
				enemy_type = level_data['enemies']['data'][i]['type']
				position_y, position_x = level_data['enemies']['data'][i]['position']
				#while True: 
				move_horisontally = bool(random.getrandbits(1))
				delta = random.choice([0, 1, 1, 1])
				change_angle = random.choice([True, False, False, False, False, False, False, False]) #bool(random.getrandbits(1))
				angle = level_data['enemies']['data'][i]['angle']
				new_position_y, new_position_x = position_y, position_x
				if change_angle:
					angle = random.choice([0, 90, -90, 180])
				if angle == 0: 
					new_position_y -= delta
				elif angle == 90: 
					new_position_x -= delta
				elif angle == -90: 
					new_position_x += delta
				elif angle == 180:
					new_position_y += delta
					
				level_data['enemies']['data'][i]['angle'] = angle
				
				# level_data['enemies']['data'][i]['position'] = (position_y, position_x)
				# level_data['enemies']['data'][i]['angle'] = angle
								
				user_position_y, user_position_x = level_data['user']['position']
				user_size_y, user_size_x = level_data['user']['size']
				if (new_position_y >= user_position_y - 1) and (new_position_y < user_position_y + user_size_y) \
						and (new_position_x >= user_position_x - 1) and (new_position_x < user_position_x + user_size_x): 
					new_position_y, new_position_x = position_y, position_x
				
				if (new_position_y >= 0) and (new_position_y <= get_map_height(level_data) - get_enemy_tank_height(level_data, enemy_type)) \
						and (new_position_x >= 0) and (new_position_x <= get_map_width(level_data) - get_enemy_tank_width(level_data, enemy_type)):
						can_be_moved = True
						for y in range(new_position_y, new_position_y + get_enemy_tank_height(level_data, enemy_type)):
							for x in range(new_position_x, new_position_x + get_enemy_tank_width(level_data, enemy_type)): 
								map_cell = level_data['map']['data'][y][x]
								if map_cell in level_data['block_types'] \
										and not level_data['block_types'][map_cell]['is_transparent']:
									can_be_moved = False
									break 
						if can_be_moved:
							position_y, position_x = new_position_y, new_position_x
				level_data['enemies']['data'][i]['position'] = (position_y, position_x)
				enemy_try_shoot(level_data, i)
				
	return level_data

def enemy_try_shoot(level_data, enemy_id):
	has_shot = random.choice([False, False, False, False, False, False, False, False, False, True])
	if has_shot:
		if level_data['enemies']['data'][enemy_id]['bullet']['position'] is None: 
			level_data['enemies']['data'][enemy_id]['bullet']['position'] = level_data['enemies']['data'][enemy_id]['position']
			level_data['enemies']['data'][enemy_id]['bullet']['angle'] = level_data['enemies']['data'][enemy_id]['angle']
	return level_data

def move_enemies_bullets(level_data):
	if level_data['enemies']['generated'] is not None:
		if len(level_data['enemies']['data']) > 0: 
			max_amount = level_data['enemies']['max_concurrent_amount']
			for i in range(0, max_amount): 
				if level_data['enemies']['data'][i]['bullet']['position'] is not None: 
					bullet_angle = level_data['enemies']['data'][i]['bullet']['angle']
					bullet_position_y, bullet_position_x = level_data['enemies']['data'][i]['bullet']['position']
					if bullet_angle == 0: 
						bullet_position_y -= 1
					elif bullet_angle == 90: 
						bullet_position_x -= 1
					elif bullet_angle == 180:
						bullet_position_y += 1
					elif bullet_angle == -90:
						bullet_position_x += 1
					if (bullet_position_y >= 0) and (bullet_position_y < get_map_height(level_data)) \
							and (bullet_position_x >= 0) and (bullet_position_x < get_map_width(level_data)):
						can_be_moved = True
						for y in range(bullet_position_y, bullet_position_y + get_bullet_height(level_data) - 1):
							for x in range(bullet_position_x, bullet_position_x + get_bullet_width(level_data) - 1): 
								map_cell = level_data['map']['data'][y][x]
								if map_cell in level_data['block_types'] \
										and not level_data['block_types'][map_cell]['is_transparent'] \
										and level_data['block_types'][map_cell]['is_destroyable']:
									if level_data['block_types'][map_cell]['bullets_to_destroy'] <= level_data['user']['power']:
										level_data['map']['data'][y][x] = level_data['map']['space']
									can_be_moved = False
									break
						if can_be_moved:
							level_data['enemies']['data'][i]['bullet']['position'] = (bullet_position_y, bullet_position_x)
						else:
							level_data['enemies']['data'][i]['bullet']['position'] = None
					else: 
						level_data['enemies']['data'][i]['bullet']['position'] = None
					user_position_y, user_position_x = level_data['user']['position']
					user_size_y, user_size_x = level_data['user']['size']
					if (bullet_position_y >= user_position_y) and (bullet_position_y < user_position_y + user_size_y) \
							and (bullet_position_x >= user_position_x) and (bullet_position_x < user_position_x + user_size_x):
						#print "YOU WAS KILLED, LOL!!1"
						level_data['text'] = "YOU WAS KILLED, LOL!!1"
						level_data['dead'] = True
						#sys.exit(0);
					target_position_y, target_position_x = level_data['target']['position']
					target_size_y, target_size_x = level_data['target']['size']
					if (bullet_position_y >= target_position_y) and (bullet_position_y < target_position_y + target_size_y) \
							and (bullet_position_x >= target_position_x) and (bullet_position_x < target_position_x + target_size_x):
						# print "YOUR FORTRESS HAS BEEN DESTROYED. GAME OVER!"
						level_data['text'] = "YOUR FORTRESS HAS BEEN DESTROYED. GAME OVER! "
						level_data['dead'] = True
						#sys.exit(0);
	return level_data

def try_shoot(level_data):
	if level_data['user']['bullet']['position'] is None: 
		level_data['user']['bullet']['position'] = level_data['user']['position']
		level_data['user']['bullet']['angle'] = level_data['user']['angle']
	return level_data

def move_bullet(level_data):
	if level_data['user']['bullet']['position'] is not None: 
		bullet_angle = level_data['user']['bullet']['angle']
		bullet_position_y, bullet_position_x = level_data['user']['bullet']['position']
		if bullet_angle == 0: 
			bullet_position_y -= 1
		elif bullet_angle == 90: 
			bullet_position_x -= 1
		elif bullet_angle == 180:
			bullet_position_y += 1
		elif bullet_angle == -90:
			bullet_position_x += 1
		if (bullet_position_y >= 0) and (bullet_position_y < get_map_height(level_data)) \
				and (bullet_position_x >= 0) and (bullet_position_x < get_map_width(level_data)):
			can_be_moved = True
			for y in range(bullet_position_y, bullet_position_y + get_bullet_height(level_data) - 1):
				for x in range(bullet_position_x, bullet_position_x + get_bullet_width(level_data) - 1): 
					map_cell = level_data['map']['data'][y][x]
					if map_cell in level_data['block_types'] \
							and not level_data['block_types'][map_cell]['is_transparent'] \
							and level_data['block_types'][map_cell]['is_destroyable']:
						if level_data['block_types'][map_cell]['bullets_to_destroy'] <= level_data['user']['power']:
							level_data['map']['data'][y][x] = level_data['map']['space']
						can_be_moved = False
						break
			if can_be_moved:
				level_data['user']['bullet']['position'] = (bullet_position_y, bullet_position_x)
			else:
				level_data['user']['bullet']['position'] = None
		else: 
			level_data['user']['bullet']['position'] = None
		target_position_y, target_position_x = level_data['target']['position']
		target_size_y, target_size_x = level_data['target']['size']
		if (bullet_position_y >= target_position_y) and (bullet_position_y < target_position_y + target_size_y) \
				and (bullet_position_x >= target_position_x) and (bullet_position_x < target_position_x + target_size_x):
			# print "YOUR FORTRESS HAS BEEN DESTROYED. GAME OVER!"
			level_data['text'] = "YOUR FORTRESS HAS BEEN DESTROYED. GAME OVER!"
			level_data['dead'] = True
			# sys.exit(0);
		if level_data['enemies']['generated'] is not None:
			if len(level_data['enemies']['data']) > 0: 
				max_amount = level_data['enemies']['max_concurrent_amount']
				for i in range(0, max_amount): 
					enemy_position_y, enemy_position_x = level_data['enemies']['data'][i]['position']
					enemy_type = level_data['enemies']['data'][i]['type']
					enemy_size_y, enemy_size_x = level_data['enemy_types'][enemy_type]['size']
					if (bullet_position_y >= enemy_position_y) and (bullet_position_y < enemy_position_y + enemy_size_y) \
							and (bullet_position_x >= enemy_position_x) and (bullet_position_x < enemy_position_x + enemy_size_x): 
						del level_data['enemies']['data'][i]
						level_data['user']['bullet']['position'] = None
						level_data['text'] = "G-G-G-GREAT SHOT!"
						if len(level_data['enemies']['data']) == 0:
							level_data['text'] = "YOU WON! PRESS ESC TO EXIT"
							level_data['dead'] = True
							# print "YOU WON! GAME OVER."
							#sys.exit(0)
	return level_data

def handle_events(events, level_data): 
	for event in events: 
		if event.type == QUIT:
			sys.exit(0)
		if (event.type == KEYDOWN) and (event.key == K_ESCAPE): 
				sys.exit(0)
		else:
			if not level_data['dead']:
				if event.type == KEYDOWN: 
					if event.key == K_UP or event.key == K_w:
						try_move_user_tank(level_data, (-1, 0))
					elif event.key == K_DOWN or event.key == K_s:
						try_move_user_tank(level_data, (1, 0))
					elif event.key == K_LEFT or event.key == K_a:
						try_move_user_tank(level_data, (0, -1))
					elif event.key == K_RIGHT or event.key == K_d:
						try_move_user_tank(level_data, (0, 1))
					elif event.key == K_SPACE:
						try_shoot(level_data)
				elif event.type == BULLET_ANIMATION_TIMER_EVENT:
					move_bullet(level_data)
				elif event.type == ENEMY_BULLET_ANIMATION_TIMER_EVENT:
					move_enemies_bullets(level_data)
				elif event.type == TANK_GENERATOR_EVENT:
					generate_enemy_tanks(level_data)
				elif event.type == TANKS_ANIMATION_TIMER_EVENT:
					move_enemy_tanks(level_data)

def do(level_data):
	pygame.key.set_repeat(KEY_REPEAT_DELAY, KEY_REPEAT_INTERVAL)
	pygame.time.set_timer(BULLET_ANIMATION_TIMER_EVENT, MILLISECONDS_PER_FRAME)
	pygame.time.set_timer(TANK_GENERATOR_EVENT, TANK_GENERATOR_DELAY)
	pygame.time.set_timer(TANKS_ANIMATION_TIMER_EVENT, TANK_ACTION_DELAY)
	pygame.time.set_timer(ENEMY_BULLET_ANIMATION_TIMER_EVENT, MILLISECONDS_PER_FRAME)
	while True: 
		handle_events(pygame.event.get(), level_data) 
		draw(level_data)
	return level_data

def init_window(level_data):
	width = get_grid_step_width(level_data) * get_map_width(level_data)
	height = get_grid_step_height(level_data) * get_map_height(level_data)
	window = pygame.display.set_mode((width, height))
	pygame.display.set_caption(game_title + ' | ' + level_data['name'])
	level_data['text'] = level_data['name']
	level_data['dead'] = False
	return level_data

def main():
	pygame.init()
	levels = load_level_list()
	for level_name in levels:
		print "> ", level_name
	user_level_name = raw_input("Level: ")
	try:
		level_data = load_level(user_level_name)
	except IOError: 
		print "Sorry, cannot load this level."
		sys.exit(0)
	init_window(level_data)
	load_level_object_images(level_data)
	do(level_data)

if __name__ == '__main__': main()
